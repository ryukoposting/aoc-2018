import Data.List
import Data.Char

{-# LANGUAGE TemplateHaskell #-}
import ManhatBox

bigBox = manhattanBox 500 :: [[Int]]

type Coord = (Int, Int)

data CalcPoint = CalcPoint (Char) (Coord)
instance Show CalcPoint where
    show (CalcPoint c (x, y)) = [c]
instance Eq CalcPoint where
    (CalcPoint c1 _) == (CalcPoint c2 _) = (c1 == c2)

type Weight = Int
type Map = [[(CalcPoint, Weight)]]

replace :: Int -> a -> [a] -> [a]
replace i n [] = []
replace i n (x:xs)
    | i == 0 = (n:xs)
    | otherwise = x:replace (i - 1) n xs

replace2 :: Int -> Int -> a -> [[a]] -> [[a]]
replace2 _ _ _ [] = []
replace2 x y n xss = replace y (replace x n $ xss !! y) xss

atLeast :: Integral a => a -> a -> a
atLeast m y
    | y < m = m
    | otherwise = y

space :: Int -> [String] -> String
space _ [] = ""
space n (x:xs) = x ++ (take fill $ repeat ' ') ++ (space n xs)
    where fill = atLeast 0 $ n - length x

-- remove n rows from the top of the table
dropTop :: Int -> [[a]] -> [[a]]
dropTop n xxs = drop n xxs

dropLeft :: Int -> [[a]] -> [[a]]
dropLeft n xxs = map (drop n) xxs

padTop :: Int -> [[Int]] -> [[Int]]
padTop n (xx:xxs) = (take n $ repeat $ take (length xx) $ repeat 9999) ++ (xx:xxs)

padBottom :: Int -> [[Int]] -> [[Int]]
padBottom n (xx:xxs) = (xx:xxs) ++ (take n $ repeat $ take (length xx) $ repeat 9999)

padLeft :: Int -> [[Int]] -> [[Int]]
padLeft n [] = []
padLeft n (xx:xxs) = [(take n $ repeat 9999) ++ xx] ++ (padLeft n xxs)

padRight :: Int -> [[Int]] -> [[Int]]
padRight n [] = []
padRight n (xx:xxs) = [xx ++ (take n $ repeat 9999)] ++ (padRight n xxs)

shiftLeft :: Int -> [[Int]] -> [[Int]]
shiftLeft n xxs
    | n >= 0    = dropLeft n xxs
    | otherwise = padLeft (abs n) xxs

shiftUp :: Int -> [[Int]] -> [[Int]]
shiftUp n xxs
    | n >= 0    = dropTop n xxs
    | otherwise = padTop (abs n) xxs

shiftRight n xxs = shiftLeft (0 - n) xxs

shiftDown n xxs = shiftUp (0 - n) xxs

centerAtCoord :: Int -> Int -> [[Int]] -> [[Int]]
centerAtCoord x y [] = []
centerAtCoord x y (xx:xxs) = shiftLeft (cx - x) . shiftUp (cy - y) $ (xx:xxs)
    where cx = div (length xx) 2
          cy = div (length (xx:xxs)) 2

chopAt :: Int -> Int -> [[Int]] -> [[Int]]
chopAt x y [] = []
chopAt x y (xx:xxs) = ((take x $ xx):(take (y - 1) $ chopAt x (y - 1) xxs))

fillTo :: Int -> Int -> [[Int]] -> [[Int]]
fillTo _ _ [] = []
fillTo x y (xx:xxs) = (padRight pr . padBottom pb) (xx:xxs)
    where pr = atLeast 0 $ x - (length xx)
          pb = atLeast 0 $ y - (length (xx:xxs))

putDimension x y = fillTo x y . chopAt x y

coord :: CalcPoint -> Coord
coord (CalcPoint _ (x, y)) = (x, y)

weightTable :: Int -> Int -> CalcPoint -> [[Weight]]
weightTable w h cp = putDimension w h $ uncurry centerAtCoord (coord cp) bigBox

splat :: (a, [b]) -> [(a, b)]
splat (x, y) = zip (repeat x) y

pivot :: [[a]] -> [[a]]
pivot ((y:ys):xs) = (map head ((y:ys):xs)):(pivot $ map tail ((y:ys):xs))
pivot _ = []

makeMapList :: Int -> Int -> [CalcPoint] -> [Map]
makeMapList w h cps = map pivot $ map (map splat) (pivot $ map (\cp -> splat (cp, (weightTable w h) cp)) cps)

flattenMapList :: [Map] -> Map
flattenMapList ls = (map . map) leaveOnlyMin ls

leaveOnlyMin :: [(CalcPoint, Weight)] -> (CalcPoint, Weight)
leaveOnlyMin xs = case filter (\(c,w) -> w == mn) xs of
                       (a:[]) -> a
                       _ -> (CalcPoint '.' (-999999, -999999), 9999)
    where mn = minimum $ map snd xs

capsIfHome :: Int -> Int -> (CalcPoint, Weight) -> (CalcPoint, Weight)
capsIfHome x y (CalcPoint a (xx,yy), b) = (CalcPoint (fn a) (xx,yy), b)
    where fn = if (x == xx) && (y == yy)
          then toUpper
          else id

prettifyMap :: Map -> String
prettifyMap xxs = unlines $ map (space 2 . map show) $ yys
    where yys = p pointlist $ (map . map) fst xxs
          p = (\ps xs -> case ps of 
                               [] -> xs
                               (CalcPoint c (x, y):pps) -> 
                                    p pps $ replace2 x y (CalcPoint (toUpper c) (x, y)) xs)

isMapEdge :: Map -> CalcPoint -> Bool
isMapEdge m c = elem c $ concat $ map (map fst $) [head test, last test, map head test, map last test]

pointlist = [
    (CalcPoint 'a' (1, 1)),
    (CalcPoint 'b' (1, 6)),
    (CalcPoint 'c' (8, 3)),
    (CalcPoint 'd' (3, 4)),
    (CalcPoint 'e' (5, 5)),
    (CalcPoint 'f' (8, 9))
    ]

calcPoint x = CalcPoint x (0,0)

test = flattenMapList $ makeMapList 10 10 pointlist

main = putStr $ prettifyMap test
