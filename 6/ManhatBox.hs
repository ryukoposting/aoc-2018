module ManhatBox where
import Data.List
import Language.Haskell.TH.Syntax

makeCol :: Integral a => a -> [a]
makeCol n = reverse lst ++ tail lst
    where lst = [0..n-1]

manhattanBox :: Integral a => a -> [[a]]
manhattanBox n = [(map fn lst) | fn <- (map (+) lst)]
    where lst = makeCol n
