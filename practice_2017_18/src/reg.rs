
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct Register {
    pub name: String
}

impl Register {
    pub fn new() -> Self
    {
        Self {
            name: String::from("")
        }
    }
}

impl Hash for Register {
    fn hash<H: Hasher>(& self, state: &mut H) { self.name.hash(state) }
}
