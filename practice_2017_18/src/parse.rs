
use reg::Register;
use regex::Regex;
use std::borrow::Borrow;

lazy_static! {
    static ref REG_RE: Regex = Regex::new("^[a-zA-Z]+$").unwrap();
    static ref LIT_RE: Regex = Regex::new("^[0-9-]+$").unwrap();
}

macro_rules! is_reg {
    ( $( $x:expr ), * ) => {
        [ $( $x ),* ].iter().all(|&k| REG_RE.is_match_at(k, 0))
    }
}

macro_rules! is_lit {
    ( $( $x:expr ), * ) => {
        [ $( $x ),* ].iter().all(|&k| LIT_RE.is_match_at(k, 0))
    }
}

macro_rules! is_op {
    ( $( $x:expr ), * ) => {
        [ $( $x ),* ].iter().all(|k| VALID_OPSTRS.contains(k))
    }
}

pub static VALID_OPSTRS: &'static [&str] = &["snd", "set", "add", "mul", "mod", "rcv", "jgz"];

#[derive(Debug, Clone)]
pub enum OpCode<'a> {
    SND(&'a Register),
    SETL(&'a Register, i32),
    SETR(&'a Register, &'a Register),
    ADDL(&'a Register, i32),
    ADDR(&'a Register, &'a Register),
    MULL(&'a Register, i32),
    MULR(&'a Register, &'a Register),
    MODL(&'a Register, i32),
    MODR(&'a Register, &'a Register),
    RCV(&'a Register),
    JGZ(&'a Register, i32),
    ABORT
}

impl<'a> OpCode<'a> {
    #[allow(dead_code)]
    pub fn snd(spl: &[&'a str], reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
    {
        if is_reg!(spl[0]) {
            reg.0.name = spl[0].to_string();
            Ok(OpCode::SND(reg.0))
        } else { Err("Invalid parameter for snd!") }
    }
    
    #[allow(dead_code)]
    pub fn rcv(spl: &[&'a str], reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
    {
        if is_reg!(spl[0]) {
            reg.0.name = spl[0].to_string();
            Ok(OpCode::RCV(reg.0))
        } else { Err("Invalid parameter for rcv!") }
    }
    
    #[allow(dead_code)]
    pub fn jgz(spl: &[&'a str], reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
    {
        if is_reg!(spl[0]) {
            if is_lit!(spl[1]) {
                reg.0.name = spl[0].to_string();
                Ok(OpCode::JGZ(reg.0, atoi(spl[1])))
            } else { Err("Invalid parameter 1 for jgz!") }
        } else { Err("Invalid parameter 0 for rcv!") }
    }
    
    #[allow(dead_code)]
    pub fn set(spl: &[&'a str], reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
    {
        if is_reg!(spl[0]) {
            if is_reg!(spl[1]) {
                reg.0.name = spl[0].to_string();
                reg.1.name = spl[1].to_string();
                Ok(OpCode::SETR(reg.0, reg.1))
            
            } else if is_lit!(spl[1]) {
                reg.0.name = spl[0].to_string();
                Ok(OpCode::SETL(reg.0, atoi(spl[1])))
            
            } else { Err("Invalid parameter 1 for set!") }
        } else { Err("Invalid parameter 0 for set!") }
    }
    
    #[allow(dead_code)]
    pub fn add(spl: &[&'a str], reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
    {
        if is_reg!(spl[0]) {
            if is_reg!(spl[1]) {
                reg.0.name = spl[0].to_string();
                reg.1.name = spl[1].to_string();
                Ok(OpCode::ADDR(reg.0, reg.1))
            
            } else if is_lit!(spl[1]) {
                reg.0.name = spl[0].to_string();
                Ok(OpCode::ADDL(reg.0, atoi(spl[1])))
            
            } else { Err("Invalid parameter 1 for add!") }
        } else { Err("Invalid parameter 0 for add!") }
    }
    
    #[allow(dead_code)]
    pub fn mul(spl: &[&'a str], reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
    {
        if is_reg!(spl[0]) {
            if is_reg!(spl[1]) {
                reg.0.name = spl[0].to_string();
                reg.1.name = spl[1].to_string();
                Ok(OpCode::MULR(reg.0, reg.1))
            
            } else if is_lit!(spl[1]) {
                reg.0.name = spl[0].to_string();
                Ok(OpCode::MULL(reg.0, atoi(spl[1])))
            
            } else { Err("Invalid parameter 1 for set!") }
        } else { Err("Invalid parameter 0 for set!") }
    }
    
    #[allow(dead_code)]
    pub fn mod_(spl: &[&'a str], reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
    {
        if is_reg!(spl[0]) {
            if is_reg!(spl[1]) {
                reg.0.name = spl[0].to_string();
                reg.1.name = spl[1].to_string();
                Ok(OpCode::MODR(reg.0, reg.1))
            
            } else if is_lit!(spl[1]) {
                reg.0.name = spl[0].to_string();
                Ok(OpCode::MODL(reg.0, atoi(spl[1])))
            
            } else { Err("Invalid parameter 1 for add!") }
        } else { Err("Invalid parameter 0 for add!") }
    }
}

fn getop<'a>(spl: Vec<&'a str>, reg: (&'a mut Register, &'a mut Register)) -> Result<OpCode<'a>, &'a str>
{
    if spl.len() < 1 { return Err("Not enough parameters provided!"); }
    
    match spl[0] {
        "snd" =>    if spl.len() == 2 { OpCode::snd(&spl[1..], reg) }
                    else { Err("Incorrect number of parameters for snd!") }
        "set" =>    if spl.len() == 3 { OpCode::set(&spl[1..], reg) }
                    else { Err("Incorrect number of parameters for set!") }
        "add" =>    if spl.len() == 3 { OpCode::add(&spl[1..], reg) }
                    else { Err("Incorrect number of parameters for add!") }
        "mul" =>    if spl.len() == 3 { OpCode::mul(&spl[1..], reg) }
                    else { Err("Incorrect number of parameters for mul!") }
        "mod" =>    if spl.len() == 3 { OpCode::mod_(&spl[1..], reg) }
                    else { Err("Incorrect number of parameters for mod!") }
        _ => Err("Token not recognized")
    }
}

pub fn parseln<'a>(s: &'a str, reg: (&'a mut Register, &'a mut Register)) -> OpCode<'a>
{
    let spl = s.split(' ');
    return match spl.clone().nth(0) {
        None => {
            println!("invalid line! {}", s);
            OpCode::ABORT
        }
        Some(_) => { 
            match getop(spl.collect(), reg) {
                Ok(opcode) => opcode,
                Err(msg) => {
                    println!("{}", msg);
                    OpCode::ABORT
                }
            }
        }
    }
}

fn atoi(s: &str) -> i32 { i32::from_str_radix(s, 10).unwrap() }
