#[macro_use] extern crate lazy_static;
extern crate regex;

#[macro_use] mod parse;
use parse::VALID_OPSTRS;
mod reg;

use std::thread;
use std::sync::Arc;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};

fn main() {
    let (tx, rx): (Sender<parse::OpCode>, Receiver<parse::OpCode>) = mpsc::channel();
    
    let parser = thread::spawn(move || {
    
        for s in ["snd a", "set asdf -3", "set a asdf", "add a b"].iter() {
            let mut regs = (Arc::new(reg::Register::new()), Arc::new(reg::Register::new()));
            let mut regs = (&mut Arc::try_unwrap(regs.0).unwrap(), &mut Arc::try_unwrap(regs.1).unwrap());
            let res = parse::parseln(s, regs);
            tx.send(res.clone()).unwrap();
        }
    
    });
    
    let runner = thread::spawn(move || {
        
    });
    
}
