import Data.List

makeCol :: Int -> [Int]
makeCol n = reverse lst ++ tail lst
    where lst = [1..n]

atLeast :: Int -> Int -> Int
atLeast m i
    | i > m     = i
    | otherwise = m

makeBox :: Int -> [[Int]]
makeBox n = [(map fn lst) | fn <- (map atLeast lst)]
    where lst = makeCol n


pretty n = putStr $ unlines $ map (unwords . map show) $ makeBox n
