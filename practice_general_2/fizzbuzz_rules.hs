
type FBRule = Integer -> Maybe String

fbrule :: Integer -> String -> FBRule
fbrule n s i =
    case mod i n of
         0 -> Just s
         _ -> Nothing

fizz = fbrule 3 "Fizz"
buzz = fbrule 5 "Buzz"

fbapply :: Integer -> String
fbapply i
    | lst == [] = show i
    | otherwise = concat lst
    where lst = [x | Just x <- map ($i) [fizz, buzz]]

fizzbuzz :: [Integer] -> [String]
fizzbuzz li =
    map fbapply li
