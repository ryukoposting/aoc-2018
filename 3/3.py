# part 1 is fine. part 2 is a horrible, brute-force method
# that can potentially just cause a stack overflow if your
# answer is too far down the list

def parseln(ln):
    spl = ln.split(' ')
    out = {}
    out["id"] = int(spl[0][1:])
    sld = spl[2].split(',')
    out["x"] = int(sld[0])
    out["y"] = int(sld[1][:-1])
    sad = spl[3].split('x')
    out["width"] = int(sad[0])
    out["height"] = int(sad[1])
    return out

def day3():
    if not os.path.isfile(OUTPUT_FNAME):
        print("downloading " + INPUT_URL)
        opener = urllib.request.build_opener()
        opener.addheaders.append(('Cookie', 'session=' + COOKIE))
        k = opener.open(INPUT_URL)
        with open(OUTPUT_FNAME, "w") as f:
            f.write(k.read().decode("utf-8"))
    
    with open(OUTPUT_FNAME) as f:
        lst = f.readlines()
        points = {}
        for ln in lst:
            p = parseln(ln)
            for x in range(p['x'], p['x'] + p['width']):
                for y in range(p['y'], p['y'] + p['height']):
                    idx =str(x) + "!!!" + str(y)
                    if idx in points:
                        points[idx].append(p)
                    else:
                        points[idx] = [p]
        
        possibles = []
        count = 0
        for a in points.keys():
            if len(points[a]) == 1:
                possibles.append(points[a][0])
            else:
                count += 1
        
        print("part 1: " + str(count))
                
        def filterlist(ls, i):
            outa = []
            outb = []
            for n in ls:
                if n['id'] == i: outa.append(n)
                else: outb.append(n)
            return(outa, outb)
        
        def dosearch(ls):
            i = ls[0]
            spl = [x for x in ls if x['id'] == i['id']]
            le = len(spl)
            w = i['width']
            h = i['height']
            if len(spl) == (i['width'] * i['height']):
                print("part 2: " + str(i['id']))
                quit()
            else:
                dosearch([x for x in ls if x['id'] != i['id']])
        
        dosearch(possibles)
        assert False
