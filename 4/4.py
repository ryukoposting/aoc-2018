def testwithout(foo):
    outstr = ''
    doagain = True
    while doagain:
        lastchr = ''
        outstr = ''
        doagain = False
        for ch in foo:
            if lastchr.upper() == ch.upper() and lastchr != ch:
                ch = ''
                doagain = True
            else:
                outstr += lastchr
            lastchr = ch
        if outstr[-1].upper() == lastchr.upper() and lastchr != outstr[-1]:
            doagain = True
            pass
        else:
            outstr += lastchr
        foo = outstr
    return len(outstr)
    
def day5():
    if not os.path.isfile(OUTPUT_FNAME):
        print("downloading " + INPUT_URL)
        opener = urllib.request.build_opener()
        opener.addheaders.append(('Cookie', 'session=' + COOKIE))
        k = opener.open(INPUT_URL)
        with open(OUTPUT_FNAME, "w") as f:
            f.write(k.read().decode("utf-8"))
    
    with open(OUTPUT_FNAME) as f:
        foo = f.read().replace('\n','')
        
        print('part 1={}'.format(testwithout(foo)))
        
        
        winchar = ''
        scores = []
        
        def ex(foo,c):
            #tw = testwithout(foo.replace(c,'').replace(c.upper(),''))
            r = re.compile('[{}{}]*'.format(c,c.upper()))
            tw = testwithout(r.sub(foo,''))
            scores.append((c,tw))
        
        NUM_THREADS = 4
        chrs = "qwertyuiopasdfghjklzxcvbnm"
        for s in [chrs[j:(j+NUM_THREADS)] for j in range(0, len(chrs), NUM_THREADS)]:
            print('starting batch of threads: {}'.format(s))
            threads = [threading.Thread(target=ex, args=(foo,c)) for c in s]
            [t.start() for t in threads]
            [t.join() for t in threads]
        
        lowest = 999999999
        lowestc = ''
        for s in scores:
            if s[1] < lowest:
                lowest = s[1]
                lowestc = s[0]
        print('part 2={} {}'.format(lowest, lowestc))
        
        
day5()
