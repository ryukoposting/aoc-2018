
#pragma once

#include <stdlib.h>

enum class Result {
    OK = 0,
    ERR = 1
};

#ifndef NDEBUG
void assert(bool expr, char const *const msg) {
    if (!expr) {
        printf("assertion failed: %s", msg);
    }
}
#else
#define assert(x) ((void)0)
#endif
