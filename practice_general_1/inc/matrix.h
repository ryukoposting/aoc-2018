
#pragma once

#include <core.h>
#include <stdio.h>
#include <stdlib.h>

namespace matrix {

    
template<typename T, int l>
struct Vector {
    
    T& operator[](int i) 
    {
        return arr[i];
    }
    
    T const operator[](int i) const
    {
        return arr[i];
    }
    
    template<int start, int len>
    struct Slice {
        
        T& operator[](int i) 
        { 
            return root[i];
        }
        
        const T& operator[](int i) const 
        {
            return root[i];
        }
        
        void copy_into(Vector<T, len> *vec)
        {
            for (int i = 0; i < len; ++i) {
                (*vec)[i] = root[i];
            }
        }
    
        Slice(T *p)
        {
            root = p + start;
        }
        
    protected:
        T *root;
    };
    
protected:
    T arr[l];
};


/* each row is c elements long */
template<typename T, int c>
struct Row {
    
    T& operator[](int i) 
    { 
        return root[i];
    }
    
    const T& operator[](int i) const 
    {
        return root[i];
    }
    
    void copy_into(Vector<T, c> *vec)
    {
        for (int i = 0; i < c; ++i) {
            (*vec)[i] = root[i];
        }
    }

    Row(T *p, int rowno)
    {
        root = p + (rowno * c);
    }
    
protected:
    T *root;
};

/* each column is r elements long */
template<typename T, int r>
struct Col {
    T& operator[](int i) 
    { 
        return root[inc * i];
    }
    
    const T& operator[](int i) const 
    {
        return root[inc * i];
    }
    
    void copy_into(Vector<T, r> *vec)
    {
        for (int i = 0; i < r; ++i) {
            (*vec)[i] = root[inc * i];
        }
    }
    
    Col(T *p, int colno, int c)
    {
        root = p + colno;
        inc = c;
    }
    
protected:
    T *root;
    int inc;
};


/* Row-oriented */
template<typename T, int r, int c>
struct Matrix {
    
    template<int colno>
    constexpr Col<T, r> col()
    {
        return Col<T, r>(arr, colno, c);
    }
    
    Col<T, r> col(int colno)
    {
        return Col<T, r>(arr, colno, c);
    }
    
    template<int rowno>
    constexpr Row<T, c> row()
    {
        return Row<T, c>(arr, rowno);
    }
    
    Row<T, c> row(int rowno)
    {
        return Row<T, c>(arr, rowno);
    }
    
    void set(int row, int col, T *const it)
    {
        arr[(row * c) + col] = *it;
    }
    
    void set(int row, int col, T it)
    {
        arr[(row * c) + col] = it;
    }
    
    T get(int row, int col)
    {
        return arr[(row * c) + col];
    }
    
private:
    T arr[c * r];
};


template<typename T, int s>
struct SquareMatrix : public Matrix<T, s, s> {};


template<typename T, int l>
constexpr void zero(Vector<T, l> &vec)
{
    for (int i = 0; i < l; ++i) vec[i] = 0;
}


template<typename T, int c, int r>
constexpr void zero(Matrix<T, r, c> &mat)
{
    for (int i = 0; i < r; ++i) {
        for (int j = 0; j < c; ++j) {
            mat.set(i, j, 0);
        }
    }
}

template<typename T, int l>
Result dot(Vector<T, l> &vec0, Vector<T, l> &vec1, T *out)
{
    *out = 0;
    for (int i = 0; i < l; ++i) {
        *out += (vec0[i] * vec1[i]);
    }
    return Result::OK;
}

template<typename T, int l>
Result dot(Row<T, l> &row, Col<T, l> &col, T *out)
{
    *out = 0;
    for (int i = 0; i < l; ++i) {
        *out += (row[i] * col[i]);
    }
    return Result::OK;
}

template<typename T, int n, int m, int p>
Result mult(Matrix<T,n,m> &mat0, Matrix<T,m,p> &mat1, Matrix<T,n,p> *out)
{
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < p; ++j) {
            T res;
            auto row = mat0.row(i);
            auto col = mat1.col(j);
            if (dot(row, col, &res) == Result::ERR) {
                return Result::ERR;
                
            } else {
                out->set(i, j, res);
            }
        }
    }
    return Result::OK;
}

template<typename T, int l>
Result cross(Vector<T, l> &vec0, Vector<T, l> &vec1, Vector<T, l> *out)
{
    if (l != 3) return Result::ERR;
    (*out)[0] = (vec0[1] * vec1[2]) - (vec0[2] * vec1[1]);
    (*out)[1] = (vec0[2] * vec1[0]) - (vec0[0] * vec1[2]);
    (*out)[2] = (vec0[0] * vec1[1]) - (vec0[1] * vec1[0]);
    return Result::OK;
}


}       // namespace matrix
