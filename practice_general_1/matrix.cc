#include <stdio.h>
#include <stdlib.h>

#include <core.h>
#include <matrix.h>

using namespace matrix;

int main()
{
    Matrix<int, 10, 10> foo;
    
    foo.set(5, 5, 25);
    
    foo.col<5>()[5] = 10;
    
    Vector<int, 10> newvec;
    foo.row<5>().copy_into(&newvec);
    
    printf("%d\n", foo.get(5, 5));
    newvec[5] = 2134;
    printf("%d\n", foo.get(5, 5));
    printf("%d\n", newvec[5]);
    
    Vector<double, 3> a;
    zero(a);
    Vector<double, 3> b;
    zero(b);
    a[0] = 1;
    a[1] = 3;
    a[2] = -5;
    b[0] = 4;
    b[1] = -2;
    b[2] = -1;
    
    double d;
    if (dot(a, b, &d) == Result::ERR) {
        printf("error\n");
    } else {
        printf("%f\n", d);
    }
    
    Matrix<double, 2, 3> mat1;
    Matrix<double, 3, 2> mat2;
    Matrix<double, 2, 2> mat3;
    
    mat1.set(0, 0, 1);  mat1.set(0, 1, 2);  mat1.set(0, 2, 3);
    mat1.set(1, 0, 4);  mat1.set(1, 1, 5);  mat1.set(1, 2, 6);
    
    mat2.set(0, 0, 7);  mat2.set(0, 1, 8);
    mat2.set(1, 0, 9);  mat2.set(1, 1, 10);
    mat2.set(2, 0, 11); mat2.set(2, 1, 12);
    
    if (mult(mat1, mat2, &mat3) == Result::ERR) {
        printf("error\n");
    } else {
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                printf("%f\t", mat3.get(i, j));
            }
            printf("\n");
        }
    }
}
